install:
	npm install -g purgecss uglifycss

mincss:
	# keep the files small
	purgecss --css css/tailwind.min.css css/bootstrap.min.css --content public/*.html --out public
	uglifycss public/tailwind.min.css public/bootstrap.min.css > public/style.css
	rm public/tailwind.min.css
	rm public/bootstrap.min.css